<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $guarded = [];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
    public function comments(){
        return $this->hasMany('App\Comment','post_id');
    }
    public function like_user(){
        return $this->belongsToMany('App\User','like_posts','user_id','post_id');
    }
}
