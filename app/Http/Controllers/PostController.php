<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Intervention\Image\Facades\Image;
use App\Post;
use Auth;
use App\Comment;
use App\User;
use DB;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function detail($id)
    {
        $post = Post::with('user')->find($id);
        $comment = Comment::where('post_id',$id)->with('user')->get();
        $comment1 = $comment->toArray();
        $jumcomment = $comment->count();
        $like_post = Post::find($id)->like_user()->count();
        $user = Auth::user();
        $com = [];
        foreach($comment as $komen){
            $komex = DB::table('like_comment')
                    ->join('users','like_comment.user_id','=','users.id')
                    ->join('comments', 'like_comment.comment_id', '=', 'comments.id')
                    ->select('*')->where('like_comment.comment_id',$komen->id)->get()->count();
            array_push($com,[
                "tot_like_komen" => $komex
            ]);
        }
        for($i =0; $i<count($comment1); $i++){
            $comment1[$i]=array_merge($comment1[$i],$com[$i]);
        }
        // dd($comment1);
        return view('post.showDetail', compact('post','user','jumcomment','like_post','comment1'));
    }
    public function tambah_komentar(Request $request, $id){
        $messages = [
            'isi.required' => 'Tulis content dulu sebelum submit'
        ];

        $request->validate([
            'isi' =>'required'
        ],$messages);
        $data = Comment::create([
            'isi' => $request['isi'],
            'user_id' => Auth::id(),
            'post_id' => $id
        ]);
        Alert::success('Congratulation', 'Komen anda telah berhasil dibuat');
        return redirect('/detail/'.$id)->with('success');
    }
    public function like_post($id){

        $user_id = Auth::id();
        $user = User::find($user_id);
        $post = Post::find($id);
        $user->like_posts()->toggle($post);
        return redirect('/detail/'.$id);
    }
    public function like_comment($post,$id){
        $user_id = Auth::id();
        $user = User::find($user_id);
        $comment = Comment::find($id);
        // $comment->like_comment()->toggle($user_id);
        $user->like_comment()->toggle($comment);
        return redirect('/detail/'.$post);
    }
    public function edit_comment($post,$id){
        $comment = Comment::find($id);
        $user = Auth::user();
        return view('/post/editComment', compact('comment','user','post'));
    }
    public function update_comment(Request $request, $post,$comment){
        $request->validate([
            'isi' => 'required'
        ]);
        $query = Comment::where('id',$comment)->update([
            'isi' => $request['isi']
        ]);
        return redirect('/detail/'.$post);
    }
    public function delete_comment($post,$comment){
        $query = Comment::destroy($comment);
        return redirect('/detail/'.$post);
    }
}
