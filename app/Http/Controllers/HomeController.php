<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use RealRashid\SweetAlert\Facades\Alert;
use Auth;
use App\User;
use App\Profil;
use App\Post;
use App\Comment;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $post = Post::with('user')->get();
        $pox = Post::with('user')->orderBy('id','DESC')->get()->toArray();
        $comment = Comment::with('post','user')->get();

        $jumkomen = [];
        foreach($post as $key=>$value){
            $k = Comment::with('user')->where('post_id',$value->id)->get()->toArray();
            $jk = Comment::with('user')->where('post_id',$value->id)->get()->count();
            $like_post = DB::table('like_posts')
                        ->join('users','like_posts.user_id','=','users.id')
                        ->join('posts', 'like_posts.post_id', '=', 'posts.id')
                        ->select('*')->where('like_posts.post_id',$value['id'])->get()->count();
            $tot_kl =[];

            foreach($k as $ke=>$v){
                $like_comment = DB::table('like_comment')
                                ->join('users','like_comment.user_id','=','users.id')
                                ->join('comments', 'like_comment.comment_id', '=', 'comments.id')
                                ->select('*')->where('like_comment.comment_id',$v['id'])->get()->count();
                array_push($tot_kl,[
                    'jum_like_komen' => $like_comment
                ]);
            }
            for($i=0; $i<$jk; $i++){
                $k[$i]=array_merge($k[$i],$tot_kl[$i]);
            }
            array_push($jumkomen, [
                'post_id' => $value->id,
                'comment' => $k,
                'total_komen' => $jk,
                'like_post' => $like_post
            ]);
        }

        for($i =0; $i<count($pox); $i++){
            $pox[$i]=array_merge($pox[$i],$jumkomen[$i]);
        }
        // dd($pox);
        // foreach($jumkomen as $key=>$value){
        //     dd($value['total_komen']);
        // }

        // foreach($jumkomen as $key=>$value){
        //     foreach($comment as $k=>$v){
        //         dd($v);
        //     }
        // }
        // dd($comment);
        //$post = DB::table('posts')->leftJoin('users', 'users.id','=','posts.user_id')->get();
        return view('home.beranda',compact('user','pox'));
    }

    public function store(Request $request){
        $messages = [
            'isi.required' => 'Tulis content dulu sebelum submit'
        ];

        $request->validate([
            'isi' =>'required'
        ],$messages);

        $user = Auth::user();
        $data = Post::create([
            'isi' => $request['isi']
        ]);
        $user->posts()->save($data);
        Alert::success('Congratulation', 'Selamat Anda Telah Membuat Konten Baru');
        return redirect('/home')->with('success');
    }
    public function tambah_komentar(Request $request, $id){
        $messages = [
            'isi.required' => 'Tulis content dulu sebelum submit'
        ];

        $request->validate([
            'isi' =>'required'
        ],$messages);
        $data = Comment::create([
            'isi' => $request['isi'],
            'user_id' => Auth::id(),
            'post_id' => $id
        ]);
        Alert::success('Congratulation', 'Komen anda telah berhasil dibuat');
        return redirect('/home')->with('success');
    }
    public function like_post($id){
        $user_id = Auth::id();
        $user = User::find($user_id);
        $post = Post::find($id);
        $user->like_posts()->toggle($post);
        return redirect('/home');
    }
    public function like_comment($id){
        $user_id = Auth::id();
        $user = User::find($user_id);
        $comment = Comment::find($id);
        $user->like_comment()->toggle($comment);
        return redirect('/home');
    }
    public function search(Request $request){
        $x = strtolower($request->search);
        $user = User::where(strtolower('name'),'like', '%'.$x.'%')->with('profil')->get();
        $x = []; $y = [];
        foreach($user as $key=>$value){
            // dd($value->id);
            $follower = DB::table('follower')
                        ->where('following_id', $value->id)->get()->count();
            $following = DB::table('follower')
                        ->where('follower_id', $value->id)->get()->count();

            array_push($x,[ 'follower' => $follower]);
            array_push($y,[ 'following' => $following]);

            // $follower = User::with('follow')->where('following_id',$value->id)->get()->count();
            // $following = User::with('follow')->where('follower_id',$value->id)->get()->count();
        }
        $user1 = $user->toArray();
        for($i=0; $i<count($user1); $i++){
            $user1[$i] = array_merge($user1[$i],$x[$i],$y[$i]);
        }
        // dd($user1);
        return view('post.users', compact('user1','request'));
    }
    public function follow($id){
        $follower = Auth::id();
        $following = User::find($id);
        $following->follow()->toggle($follower);
        return redirect('/home');
    }

}
