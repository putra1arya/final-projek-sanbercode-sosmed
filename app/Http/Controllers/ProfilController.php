<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use RealRashid\SweetAlert\Facades\Alert;
use Auth;
use App\User;
use App\Profil;
use App\Post;
use App\Comment;
use DB;



class ProfilController extends Controller
{
    public $path;
    public $dimensions;
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show_profil(){
        $user = Auth::user();
        $profil = Profil::find($user->id);
        $post = Post::with('user')->where('user_id',$user->id)->get();
        $pox = Post::with('user')->where('user_id',$user->id)->orderBy('id','DESC')->get()->toArray();
        $comment = Comment::with('post','user')->get();
        $jumkomen = [];
        foreach($post as $key=>$value){
            $k = Comment::with('user')->where('post_id',$value->id)->get()->toArray();
            $jk = Comment::with('user')->where('post_id',$value->id)->get()->count();
            $like_post = DB::table('like_posts')
                        ->join('users','like_posts.user_id','=','users.id')
                        ->join('posts', 'like_posts.post_id', '=', 'posts.id')
                        ->select('*')->where('like_posts.post_id',$value['id'])->get()->count();
            $tot_kl =[];

            foreach($k as $ke=>$v){
                $like_comment = DB::table('like_comment')
                                ->join('users','like_comment.user_id','=','users.id')
                                ->join('comments', 'like_comment.comment_id', '=', 'comments.id')
                                ->select('*')->where('like_comment.comment_id',$v['id'])->get()->count();
                array_push($tot_kl,[
                    'jum_like_komen' => $like_comment
                ]);
            }
            for($i=0; $i<$jk; $i++){
                $k[$i]=array_merge($k[$i],$tot_kl[$i]);
            }
            array_push($jumkomen, [
                'post_id' => $value->id,
                'comment' => $k,
                'total_komen' => $jk,
                'like_post' => $like_post
            ]);
        }

        for($i =0; $i<count($pox); $i++){
            $pox[$i]=array_merge($pox[$i],$jumkomen[$i]);
        }
        $follower = DB::table('follower')
                        ->where('following_id', $user->id)->get()->count();
        $following = DB::table('follower')
                        ->where('follower_id', $user->id)->get()->count();
        $jumpost = $post->count();
        // dd($pox);
        return view('post.profil', compact('user','profil','pox','follower','following','jumpost'));
    }
    public function edit_profil($id){
        $user = Auth::user();
        $profil = Profil::find($id);
        return view('post.editProfil',compact('user','profil'));
    }
    public function update_profil(Request $request, $id){
        $validasi = $this->validate($request, [
            'nama_lengkap' => 'required',
            'tempat_lahir'=> 'required',
            'tanggal_lahir' => 'required',
            'gender'=>'required',
            'deskripsi_diri'=>'required',
            'image'=> 'required|mimes:jpg,png,jpeg'

        ]);

        if($request->hasFile('image')){
            $avatar = $request->file('image');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(200,200)->save( public_path('/adminlte/upload/' .$filename));
            $user = Auth::user();
            $user->avatar = $filename;
            $user->save();
        }

        $profil = Profil::find($id);
        $profil->nama_lengkap = $request->nama_lengkap;
        $profil->tempat_lahir = $request->tempat_lahir;
        $profil->tanggal_lahir = $request->tanggal_lahir;
        $profil->gender = $request->gender;
        $profil->deskripsi_diri = $request->deskripsi_diri;
        $profil->save();

        return redirect('/profil')->with('success', 'Data berhasil di update');

    }
    public function tambah_post(Request $request){
        $messages = [
            'isi.required' => 'Tulis content dulu sebelum submit'
        ];

        $request->validate([
            'isi' =>'required'
        ],$messages);

        $user = Auth::user();
        $data = Post::create([
            'isi' => $request['isi']
        ]);
        $user->posts()->save($data);
        Alert::success('Congratulation', 'Selamat Anda Telah Membuat Konten Baru');
        return redirect('/profil')->with('success');
    }
    public function tambah_komentar(Request $request, $id){
        $messages = [
            'isi.required' => 'Tulis content dulu sebelum submit'
        ];

        $request->validate([
            'isi' =>'required'
        ],$messages);
        $data = Comment::create([
            'isi' => $request['isi'],
            'user_id' => Auth::id(),
            'post_id' => $id
        ]);
        Alert::success('Congratulation', 'Komen anda telah berhasil dibuat');
        return redirect('/profil')->with('success');
    }
    public function like_post($id){
        $user_id = Auth::id();
        $user = User::find($user_id);
        $post = Post::find($id);
        $user->like_posts()->toggle($post);
        return redirect('/profil');
    }
    public function like_comment($id){
        $user_id = Auth::id();
        $user = User::find($user_id);
        $comment = Comment::find($id);
        $user->like_comment()->toggle($comment);
        return redirect('/profil');
    }
    public function destroy($id){
        $post = Post::destroy($id);
        return redirect('/profil');
    }
    public function edit_post($id){
        $post = Post::find($id);
        $user = Auth::user();
        return view('post.editPost',compact('post','user'));
    }
    public function update_post(Request $request,$id){
        $request->validate([
            'isi' => 'required'
        ]);
        $query = Post::where('id',$id)->update([
            'isi' => $request['isi']
        ]);
        return redirect('/profil');
    }
    public function get_other_user($id){
        $xUser = Auth::user();
        $user = User::find($id);
        $profil = Profil::find($id);
        $post = Post::with('user')->where('user_id',$id)->get();
        $pox = Post::with('user')->where('user_id',$id)->orderBy('id','DESC')->get()->toArray();
        $comment = Comment::with('post','user')->get();
        $jumkomen = [];
        foreach($post as $key=>$value){
            $k = Comment::with('user')->where('post_id',$value->id)->get()->toArray();
            $jk = Comment::with('user')->where('post_id',$value->id)->get()->count();
            $like_post = DB::table('like_posts')
                        ->join('users','like_posts.user_id','=','users.id')
                        ->join('posts', 'like_posts.post_id', '=', 'posts.id')
                        ->select('*')->where('like_posts.post_id',$value['id'])->get()->count();
            $tot_kl =[];

            foreach($k as $ke=>$v){
                $like_comment = DB::table('like_comment')
                                ->join('users','like_comment.user_id','=','users.id')
                                ->join('comments', 'like_comment.comment_id', '=', 'comments.id')
                                ->select('*')->where('like_comment.comment_id',$v['id'])->get()->count();
                array_push($tot_kl,[
                    'jum_like_komen' => $like_comment
                ]);
            }
            for($i=0; $i<$jk; $i++){
                $k[$i]=array_merge($k[$i],$tot_kl[$i]);
            }
            array_push($jumkomen, [
                'post_id' => $value->id,
                'comment' => $k,
                'total_komen' => $jk,
                'like_post' => $like_post
            ]);
        }

        for($i =0; $i<count($pox); $i++){
            $pox[$i]=array_merge($pox[$i],$jumkomen[$i]);
        }
        $follower = DB::table('follower')
                        ->where('following_id', $user->id)->get()->count();
        $following = DB::table('follower')
                        ->where('follower_id', $user->id)->get()->count();
        $jumpost = $post->count();
        // dd($pox);
        return view('post.profilOther', compact('user','xUser','profil','pox','like_post','follower','following','jumpost'));
    }
    public function follow($id){
        $follower = Auth::id();
        $following = User::find($id);
        $following->follow()->toggle($follower);
        return redirect('/profil_user/'.$id);
    }
    public function comment_other_profil(Request $request , $post, $userid){
        $messages = [
            'isi.required' => 'Tulis content dulu sebelum submit'
        ];

        $request->validate([
            'isi' =>'required'
        ],$messages);
        $data = Comment::create([
            'isi' => $request['isi'],
            'user_id' => Auth::id(),
            'post_id' => $post
        ]);
        Alert::success('Congratulation', 'Komen anda telah berhasil dibuat');
        return redirect('/profil_user/'.$userid)->with('success');
    }
    public function like_other_post($id,$userid){
        $user_id = Auth::id();
        $user = User::find($user_id);
        $post = Post::find($id);
        $user->like_posts()->toggle($post);
        return redirect('/profil_user/'.$userid);
    }
    public function like_other_comment($id,$userid){
        $user_id = Auth::id();
        $user = User::find($user_id);
        $comment = Comment::find($id);
        $user->like_comment()->toggle($comment);
        return redirect('/profil_user/'.$userid);
    }
}
