@extends('layout.master')
@push('script-head')
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
@endpush
@section('content')
<div class="col-2">
</div>
<div class="col-1"></div>
    <div class="col-6">
        <br>
        <div class="card card-warning">
            <div class="card-header">
            <h3 class="card-title">Buat Postingan</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="/home" method="POST">
                @csrf
            <div class="card-body">
                <div class="user-block">
                    <img class="img-circle" src="{{asset('adminlte/upload/'.$user->avatar)}}" alt="User Image">
                    <span class="username"><a href="/profil">{{$user->name}}</a></span>
                </div>
                <div class="form-group">
                    <br><br>
                    <textarea name="isi" class="form-control my-editor">{!! old('content','') !!}</textarea>

                </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
    </div>
    @error('isi')
         <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <br><h3>Current Post :  </h3>

    <?php
        foreach ($pox as $p => $value) {
    ?>
            <div class="card card-widget card-warning">
                <div class="card-header">
                    <div class="user-block">
                        <img class="img-circle" src="{{asset('adminlte/upload/'.$value['user']['avatar'])}}" alt="User Image">
                        @if ($user->id == $value['user']['id'])
                            <span class="username"><a href="/profil">{{$value['user']['name']}}</a></span>
                        @else
                            <span class="username"><a href="/profil_user/{{$value['user']['id']}}">{{$value['user']['name']}}</a></span>
                        @endif

                        <span class="description"> {{$value['created_at']}}</span>
                    </div>
                </div>
                <div class="card-body" style="display: block;">
                    {!!$value['isi']!!}
                    <a href="/like_beranda/{{$value['id']}}" class="btn btn-default btn-sm"><i class="far fa-thumbs-up"></i> Like</a>
                    <a href="/detail/{{$value['id']}}" class="btn btn-default btn-sm"><i class="far fa-comment"></i> Comment</a>
                    <span class="float-right text-muted">{{$value['like_post']}} likes - {{$value['total_komen']}} comments</span>
                </div>
                @forelse ($value['comment'] as $k=>$v)

                <div class="card-footer card-comments" style="display: block;">
                    <div class="card-comment">
                        <img class="img-circle img-sm" src="{{asset('adminlte/upload/'.$v['user']['avatar'])}}" alt="User Image">
                        <div class="comment-text">
                            @if ($user->id == $v['user']['id'])
                                <span class="username">
                                    <a href="/profil">{{$v['user']['name']}}</a>
                                    <span class="text-muted float-right">{{$v['created_at']}}</span>
                                </span>
                            @else
                                <span class="username">
                                    <a href="/profil_user/{{$v['user']['id']}}">{{$v['user']['name']}}</a>
                                    <span class="text-muted float-right">{{$v['created_at']}}</span>
                                </span>
                            @endif
                            {!!$v['isi']!!}
                        </div>
                        <div class="d-flex">
                            <a href="/like_comment/{{$v['id']}}" style="color: grey; margin : 10px"><i class="far fa-thumbs-up"></i> Like</a>
                            <span class="ml-auto" style="margin-left :40px; color: grey;">{{$v['jum_like_komen']}} likes</span>
                        </div>

                    </div>
                </div>
                {{-- @endforeach --}}

                @empty
                <p style="text-align:center; ">Belum Ada Komentar</p>
                @endforelse
                <div class="card-footer" style="display: block;">
                    <form action="/home/{{$value['id']}}" method="post">
                      @csrf
                      <img class="img-fluid img-circle img-sm" src="{{asset('adminlte/upload/'.$user->avatar)}}" alt="Alt Text">
                      <!-- .img-push is used to add margin to elements next to floating images -->
                      <div class="img-push">
                        <input type="text" name="isi" class="form-control form-control-sm" placeholder="Press enter to post comment">
                      </div>
                    </form>
                </div>
            </div>
    <?php } ?>
                {{-- comment --}}
                <?php
                    //foreach ($comment as $komen => $kom) { ?>
                        {{-- <div class="card-footer card-comments" style="display: block;">
                            <div class="card-comment">
                                <img class="img-circle img-sm" src="{{asset('adminlte/dist/img/user1-128x128.jpg')}}" alt="User Image">
                                <div class="comment-text">
                                    <span class="username">
                                        {{$kom->user->name}}
                                        <span class="text-muted float-right">{{$kom->created_at}}</span>
                                    </span>
                                    {!!$kom->isi!!}
                                </div>
                                <span style="margin-left :40px; color: grey;">200<a href="#" style="color: grey; margin : 10px"><i class="far fa-thumbs-up"></i> Like</a></span>
                            </div> --}}
                <?php //} ?>
                {{-- <div class="card-footer" style="display: block;">
                    <form action="/home/{{$value->id}}" method="post">
                      @csrf
                      <img class="img-fluid img-circle img-sm" src="{{asset('adminlte/dist/img/user1-128x128.jpg')}}" alt="Alt Text">
                      <!-- .img-push is used to add margin to elements next to floating images -->
                      <div class="img-push">
                        <input type="text" name="isi" class="form-control form-control-sm" placeholder="Press enter to post comment">
                      </div>
                    </form>
                </div>
            </div> --}}


@endsection

@push('script')
<script>

    var editor_config = {
      path_absolute : "/",
      selector: "textarea.my-editor",
      plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern"
      ],
      toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
      relative_urls: false,
      file_browser_callback : function(field_name, url, type, win) {
        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

        var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
        if (type == 'image') {
          cmsURL = cmsURL + "&type=Images";
        } else {
          cmsURL = cmsURL + "&type=Files";
        }

        tinyMCE.activeEditor.windowManager.open({
          file : cmsURL,
          title : 'Filemanager',
          width : x * 0.8,
          height : y * 0.8,
          resizable : "yes",
          close_previous : "no"
        });
      }
    };

    tinymce.init(editor_config);
  </script>
@endpush
