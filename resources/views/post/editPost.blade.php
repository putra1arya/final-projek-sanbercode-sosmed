@extends('layout.master')

@push('script-head')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
@endpush
@section('content')
<div class="col-2">
</div>
<div class="col-1"></div>
<div class="col-6"><br><br>
    <div class="card card-warning">
        <div class="card-header">
        <h3 class="card-title">Buat Postingan</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/profil_post/{{$post->id}}" method="POST">
            @csrf
            @method('PUT')
        <div class="card-body">
            <div class="user-block">
                <img class="img-fluid img-circle img-sm" src="{{asset('adminlte/upload/'.$user->avatar)}}" alt="Alt Text">
                <span class="username"><a href="#">{{$user->name}}</a></span>
            </div>
            <div class="form-group">
                <br><br>
                <textarea name="isi" class="form-control my-editor">{!! old('isi', $post->isi) !!}</textarea>

            </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </form>
    </div>
    </div>
    @error('isi')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

@endsection

@push('script')
<script>

    var editor_config = {
      path_absolute : "/",
      selector: "textarea.my-editor",
      plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern"
      ],
      toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
      relative_urls: false,
      file_browser_callback : function(field_name, url, type, win) {
        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

        var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
        if (type == 'image') {
          cmsURL = cmsURL + "&type=Images";
        } else {
          cmsURL = cmsURL + "&type=Files";
        }

        tinyMCE.activeEditor.windowManager.open({
          file : cmsURL,
          title : 'Filemanager',
          width : x * 0.8,
          height : y * 0.8,
          resizable : "yes",
          close_previous : "no"
        });
      }
    };

    tinymce.init(editor_config);
  </script>
@endpush
