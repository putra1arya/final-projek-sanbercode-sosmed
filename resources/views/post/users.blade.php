@extends('layout.master')

@section('content')
<div class="col-2">
</div>
<div class="col-1"></div>
<div class="col-6">
    <br>

        @forelse ($user1 as $key=>$value)
        <br>
        <div class="card card-primary card-outline">
            <div class="card-body box-profile">
            <div class="text-center">
                <img class="profile-user-img img-fluid img-circle" src="{{asset('adminlte/upload/'.$value['avatar'])}}" alt="User profile picture">
            </div>

            <h3 class="profile-username text-center"><a href="/profil_user/{{$value['id']}}">{{$value['profil']['nama_lengkap']}}</a></h3>

            <p class="text-muted text-center">{{'@'}}{{$value['name']}}</p>

            <ul class="list-group list-group-unbordered mb-3">
                <li class="list-group-item">
                <b>Followers</b> <a class="float-right">{{$value['follower']}}</a>
                </li>
                <li class="list-group-item">
                <b>Following</b> <a class="float-right">{{$value['following']}}</a>
                </li>
                <li class="list-group-item">
            </ul>

            <a href="/follow/{{$value['id']}}" class="btn btn-primary btn-block"><b>Follow</b></a>
            </div>
            <!-- /.card-body -->
        </div>


@empty

<div class="callout callout-danger">
    <h5>TIDAK ADA USER DENGAN NAMA {{$request->search}} !</h5>
    <p>Silahkan Cari Lagi dengan <b>Username</b> lain</p>
</div>
@endforelse
</div>

@endsection
