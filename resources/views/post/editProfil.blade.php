@extends('layout.master')

    @section('content')

	<div class="col-2">
	</div>
	<div class="col-1"></div>
	<div class="col-md-6">
	    <br>
	    <br>
	    <div class="card">
	        <div class="card-header">{{ __('Edit Profil') }}</div>

	        <div class="card-body">
	            <form method="POST" action="/profil/{{$profil->id}}" enctype="multipart/form-data">
	                @csrf
	                @method('PUT')
	                <div class="form-group row">
	                    <label for="nama_lengkap" class="col-md-4 col-form-label text-md-right">{{ __('Nama Lengkap') }}</label>

	                    <div class="col-md-6">
	                        <input id="nama_lengkap" type="text" class="form-control @error('nama_lengkap') is-invalid @enderror"name="nama_lengkap" value="{{ old('nama_lengkap',$profil->nama_lengkap) }}" required autocomplete="nama_lengkap" autofocus>

	                        @error('nama_lengkap')
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $message }}</strong>
	                            </span>
	                        @enderror
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="tempat_lahir" class="col-md-4 col-form-label text-md-right">{{ __('Tempat Lahir') }}</label>

	                    <div class="col-md-6">
	                        <input id="tempat_lahir" type="text" class="form-control @error('tempat_lahir') is-invalid @enderror" name="tempat_lahir" value="{{ old('tempat_lahir',$profil->tempat_lahir) }}" required autocomplete="tempat_lahir" autofocus>

	                        @error('tempat_lahir')
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $message }}</strong>
	                            </span>
	                        @enderror
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="tanggal_lahir" class="col-md-4 col-form-label text-md-right">{{ __('Tanggal Lahir') }}</label>

	                    <div class="col-md-6">
	                        <input id="tanggal_lahir" type="date" class="form-control @error('tanggal_lahir') is-invalid @enderror" name="tanggal_lahir" value="{{ old('tanggal_lahir',$profil->tanggal_lahir) }}" required autocomplete="tanggal_lahir" autofocus>

	                        @error('tanggal_lahir')
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $message }}</strong>
	                            </span>
	                        @enderror
	                    </div>
	                </div>

	                <div class="form-group row" value="{{ old('gender',$profil->gender) }}">
	                    <label for="Gender" class="col-md-4 col-form-label text-md-right">{{ __('Gender') }}</label>
	                    <div class="form-check col-md-6">
	                        <input class="form-check-input" type="radio" name="gender" value="Laki-Laki" {{ old('gender') == "Laki-Laki" ? 'checked' : '' }}>
	                        <label class="form-check-label">Laki-laki</label>
	                    </div>
	                    <label for="Gender" class="col-md-4 col-form-label text-md-right"></label>
	                    <div class="form-check col-md-6">
	                        <input class="form-check-input" type="radio" name="gender" value="Perempuan" {{ old('gender') == "Perempuan" ? 'checked' : '' }}>
	                        <label class="form-check-label">Perempuan</label>
	                        @error('gender')
	                                <span class="invalid-feedback" role="alert">
	                                    <strong>{{ $message }}</strong>
	                                </span>
	                        @enderror
	                    </div>
	                </div>

	                <div class="form-group row">
	                    <label for="deskripsi_diri" class="col-md-4 col-form-label text-md-right">{{ __('Deskripsi Diri') }}</label>

	                    <div class="col-md-6">
                            <textarea name="deskripsi_diri" id="deskripsi_diri"  class="form-control @error('deskripsi_diri') is-invalid @enderror">{{ old('deskripsi_diri',$profil->deskripsi_diri) }}</textarea>

	                        @error('deskripsi_diri')
	                            <span class="invalid-feedback" role="alert">
	                                <strong>{{ $message }}</strong>
	                            </span>
	                        @enderror
	                    </div>
                    </div>
                    <div class="form-group row">
	                    <label for="image" class="col-md-4 col-form-label text-md-right">{{ __('Foto Profil Anda') }}</label>
	                    <div class="col-md-6">
                            <input type="file" name="image" id="image">
                        @error('image')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
	                    </div>
	                </div>
	                <div class="form-group row mb-0">
	                    <div class="col-md-6 offset-md-4">
	                        <button type="submit" value="upload" class="btn btn-primary">
	                            {{ __('Update Profil') }}
	                        </button>
	                    </div>
	                </div>
	            </form>
	        </div>
	    </div>
    </div>
	@endsection
