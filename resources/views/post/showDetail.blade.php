@extends('layout.master')
@section('content')
<div class="col-2">
</div>
<div class="col-1"></div>
    <div class="col-6">
        <br>
        <div class="card card-widget card-warning">
            <div class="card-header">
                <div class="user-block">
                    <img class="img-fluid img-circle img-sm" src="{{asset('adminlte/upload/'.$post->user->avatar)}}" alt="Alt Text">
                    @if ($user->id == $post->user->id)
                        <span class="username"><a href="/profil">{{$post->user->name}}</a></span>
                    @else
                        <span class="username"><a href="/profil_user/{{$post->user->id}}">{{$post->user->name}}</a></span>
                    @endif

                    <span class="description"> {{$post->created_at}}</span>
                </div>
            </div>
            <div class="card-body" style="display: block;">
                {!!$post->isi!!}
                <a href="/like/{{$post->id}}" class="btn btn-default btn-sm"><i class="far fa-thumbs-up"></i> Like</a>
                <span class="float-right text-muted">{{$like_post}} likes - {{$jumcomment}} comments</span>
            </div>
        </div>

    {{-- comment --}}
    @forelse ($comment1 as $key=>$value)
        <div class="card-footer card-comments card" style="display: block; background:white;">
            <div class="card-comment">
                <img class="img-fluid img-circle img-sm" src="{{asset('adminlte/upload/'.$value['user']['avatar'])}}" alt="Alt Text">
                <div class="comment-text">
                    @if ($user->id == $value['user']['id'])
                        <span class="username">
                            <a href="/profil">{{$value['user']['name']}}</a>
                            <span class="text-muted float-right">{{$value['created_at']}}</span>
                        </span>
                    @else
                        <span class="username">
                            <a href="/profil_user/{{$value['user']['id']}}">{{$value['user']['name']}}</a>
                            <span class="text-muted float-right">{{$value['created_at']}}</span>
                        </span>
                    @endif

                </div>
                <br>
                {!!$value['isi']!!}
                <br>
                <div class="d-flex">
                    <a href="/like_comment/{{$post->id}}/{{$value['id']}}" style="color: grey; margin-top : 10px"><i class="far fa-thumbs-up"></i> Like</a>
                    <span class="ml-auto" style="margin-left :40px; color: grey;">{{$value['tot_like_komen']}} likes</span>
                </div>
                <div style="display: flex; margin-top: 5px;">
                    @if ($value['user']['id']==$user->id)
                    <a href="/post/{{$value['post_id']}}/{{$value['id']}}/edit" style="color: grey; margin-top : 10px">Edit</a>
                    <form action="/post/{{$value['post_id']}}/{{$value['id']}}/delete" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="Delete" class="btn mt-1" style="color: grey">
                    </form>
                    @endif
                </div>
            </div>
        </div>
    @empty
    <p style="text-align:center; ">Belum Ada Komentar</p>
    @endforelse

        <div class="card-footer card" style="display: block;">
            <form action="/detail/{{$post->id}}" method="post">
            @csrf
            <img class="img-fluid img-circle img-sm" src="{{asset('adminlte/upload/'.$user->avatar)}}" alt="Alt Text">
            <!-- .img-push is used to add margin to elements next to floating images -->
            <div class="img-push">
                <input type="text" name="isi" class="form-control form-control-sm" placeholder="Press enter to post comment">
            </div>
            </form>
        </div>
    </div>



@endsection
