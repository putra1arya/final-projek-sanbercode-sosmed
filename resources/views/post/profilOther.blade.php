@extends('layout.master')

@push('script-head')
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
@endpush

@section('content')
        <div class="col-md-1"></div>
        <div class="col-md-3"><br>
          <!-- About Me Box -->
          <div class="card card-warning">
            <div class="card-header">
              <h3 class="card-title">About Me</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <strong><i class="fas fa-book mr-1"></i> Tempat, Tanggal Lahir</strong>

              <p class="text-muted">
                {{$profil->tempat_lahir}}, {{$profil->tanggal_lahir}}
              </p>

              <hr>

              <strong><i class="fas fa-map-marker-alt mr-1"></i> Gender</strong>

              <p class="text-muted">{{$profil->gender}}</p>

              <hr>

              <strong><i class="fas fa-pencil-alt mr-1"></i> Deskripsi Diri</strong>

              <p class="text-muted">
                {{$profil->deskripsi_diri}}
              </p>

              <hr>

            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.col -->
        <div class="col-md-6">
            <br>
            <div class="card card-widget widget-user"    >
            <!-- Add the bg color to the header using any of the bg-* classes -->

            <div class="widget-user-header bg-info bg-warning">
                <h3 class="widget-user-username">{{$profil->nama_lengkap}}</h3>
            </div>
            <div class="widget-user-image">
              <img class="img-circle elevation-2" src="{{asset('adminlte/upload/'.$user->avatar)}}" alt="User Avatar">
            </div>
            <div class="card-footer">

              <div class="row">
                <div class="col-sm-4 border-right">
                  <div class="description-block">
                  <h5 class="description-header">{{$following}}</h5>
                    <span class="description-text">FOLLOWING</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4 border-right">
                  <div class="description-block">
                    <h5 class="description-header">{{$follower}}</h5>
                    <span class="description-text">FOLLOWERS</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4">
                  <div class="description-block">
                    <h5 class="description-header">{{$jumpost}}</h5>
                    <span class="description-text">POST</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
              </div>
              <a href="/followprofil_user/{{$user->id}}" class="btn btn-warning btn-block"><b>Follow</b></a>
              <!-- /.row -->
              <br>
            </div>
          </div>

        <br><h3>{{$user->name}}'s Post :  </h3>

        <?php
        foreach ($pox as $p => $value) {
        ?>
            <div class="card card-widget card-warning">
                <div class="card-header d-flex">
                    <div class="user-block">
                        <img class="img-fluid img-circle img-sm" src="{{asset('adminlte/upload/'.$value['user']['avatar'])}}" alt="Alt Text">
                        <span class="username"><a href="#">{{$value['user']['name']}}</a></span>
                        <span class="description"> {{$value['created_at']}}</span>
                    </div>
                    <div class="ml-auto" style="display: flex;">
                        @if ($value['user']['id']==$xUser->id)
                            <a href="/profil_post/{{$value['post_id']}}/edit" style="color: white; margin-top : 10px">Edit</a>
                                <form action="/profil_post/{{$value['post_id']}}/delete" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value="Delete" class="btn mt-1" style="color: white">
                                </form>
                        @endif
                    </div>
                </div>
                <div class="card-body" style="display: block;">
                    {!!$value['isi']!!}
                    <a href="/like_post_other_profil/{{$value['id']}}/{{$user->id}}" class="btn btn-default btn-sm"><i class="far fa-thumbs-up"></i> Like</a>
                    <a href="/detail/{{$value['id']}}" class="btn btn-default btn-sm"><i class="far fa-comment"></i> Comment</a>
                    <span class="float-right text-muted">{{$value['like_post']}} likes - {{$value['total_komen']}} comments</span>
                </div>
                @forelse ($value['comment'] as $k=>$v)

                <div class="card-footer card-comments" style="display: block;">
                    <div class="card-comment">
                        <img class="img-fluid img-circle img-sm" src="{{asset('adminlte/upload/'.$v['user']['avatar'])}}" alt="Alt Text">
                        <div class="comment-text">
                            <span class="username">
                                {{$v['user']['name']}}
                                <span class="text-muted float-right">{{$v['created_at']}}</span>
                            </span>
                            {!!$v['isi']!!}
                        </div>
                        <div class="d-flex">
                            <a href="/like_comment_other_profil/{{$v['id']}}/{{$user->id}}" style="color: grey; margin : 10px"><i class="far fa-thumbs-up"></i> Like</a>
                            <span class="ml-auto" style="margin-left :40px; color: grey;">{{$v['jum_like_komen']}} likes</span>
                        </div>

                    </div>
                </div>
                {{-- @endforeach --}}

                @empty
                <p style="text-align:center; ">Belum Ada Komentar</p>

                @endforelse
                <div class="card-footer" style="display: block;">
                    <form action="/comment_other_profil/{{$value['id']}}/{{$value['user']['id']}}" method="post">
                      @csrf

                      <!-- .img-push is used to add margin to elements next to floating images -->
                      <div class="img-push">
                        <input type="text" name="isi" class="form-control form-control-sm" placeholder="Press enter to post comment">
                      </div>
                    </form>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="col-2"></div>

@endsection


